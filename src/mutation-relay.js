const {events, clear} = require("alcumus-app-datasource-mutations")
const {mutateAll} = require("./data-source")
const debounce = require("lodash/debounce")

const process = debounce(function process({mutations}) {
    mutateAll({body: mutations})
    clear()
}, 100, {maxWait: 200})

function instant(...params) {
    process(...params)
}

module.exports = {
    enable() {
        events.on("mutated", instant)
    },
    disable() {
        events.removeListener("mutated", instant)
        process.flush()
    },
    flush() {
        process.flush()
    }
}

