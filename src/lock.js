const {createClient} = require("./redis")
const redisLock = require("ioredis-lock")
const lockPool = [createLock(), createLock()]

function createLock() {
    return redisLock.createLock(createClient(), {
        timeout: 60000,
        retries: 5000,
        delay: 250
    })
}

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}

async function lock(key) {
    if (lockPool.length === 0) {
        lockPool.push(createLock())
    }
    let lock = lockPool.pop()

    let retries = 0
    let ok = false
    while(!ok && retries < 500) {
        try {
            await lock.acquire(key)
            ok = true
        } catch(e) {
            retries++
            await delay(200)
        }
    }
    if(!ok) {
        throw new Error("Lock not acquired")
    }
    let result = () => {
        lock.release()
        lockPool.push(lock)
    }
    result.extend = function(time) {
        return lock.extend(time || 60000)
    }
    result.extend.lock = lock
    return result
}

async function using(key, fn) {
    let release = await module.exports.lock(key)
    try {
        return await Promise.resolve(fn(release.extend))
    } finally {
        release()
    }
}

module.exports = {lock, using}
