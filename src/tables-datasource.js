const {events} = require("./data-source")
const {query, escapeId} = require("./database")

function noop() {}

events.on("data-source.*._tables", function ({functions}) {
    Object.assign(functions, {
        get: async ({params: {database}}) => {
            let [rows] = await query(`SHOW TABLES FROM ${escapeId(database)}`)
            rows = rows.map(row => ({table: row[`Tables_in_${database}`]})).filter(row => !row.table.startsWith("_"))
            return {rows, rowCount: rows.length}
        },
        updates: noop,
        mutate: noop
    })

})
