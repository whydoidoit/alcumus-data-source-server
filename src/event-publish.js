const {get, release, redis} = require("./redis")
const DAYS_TO_KEEP_EVENTS = +(process.env.DAYS_TO_KEEP_EVENTS || 30)
const {generate} = require("shortid")
const isString = require("lodash/isString")
const {publish: pubSubPublish} = require("alcumus-pub-sub")

async function publish(event, data, database, keepPacket = DAYS_TO_KEEP_EVENTS) {
    keepPacket = days(keepPacket)
    if (!isString(event)) throw new Error("event must be the name of the event to fire")
    data = data || ""
    if (!isString(database)) throw new Error("database must be the name of a database")
    const toSend = {event, data, id: generate()}
    let publisher = await get()
    try {
        await publisher.set(`_mt_${toSend.id}`, JSON.stringify(toSend), 'ex', keepPacket)
        let ok = false
        do {
            await publisher.watch(`_last_${database}`)
            let last = await publisher.get(`_last_${database}`)
            await publisher.watch(`_next_${last}`)
            await publisher.multi({pipeline: false})
            await publisher.set(`_next_${last}`, toSend.id, 'ex', keepPacket)
            await publisher.set(`_last_${database}`, toSend.id)
            ok = !!await publisher.exec()
        } while (!ok)
        await pubSubPublish(`event.${database}`, toSend.id)
    } finally {
        release(publisher)
    }
}

function days(number) {
    return Math.floor(number * 60 * 60 * 24)
}

async function retrieve(id) {
    let data = await redis.get(`_mt_${id}`)
    if (!data) {
        data = "null"
    }
    let nextEvent = await redis.get(`_next_${id}`)
    return [JSON.parse(data), nextEvent]
}

module.exports = {retrieve, publish}

