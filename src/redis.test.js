const {createClient} = require('./redis')

describe('redis', function () {
    this.timeout(11000)
    it('should get a redis connection', async () => {
        let con = createClient('redis://127.0.0.1:6379')
        await con.set('test.test', 1)
        console.log(await con.get('test.test'))
    })
})