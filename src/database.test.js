const {where} = require('./database')
const {expect} = require('chai')

describe('Database test', function () {
    it('should be able to make a where clause from a mongo query', async function () {
        let result = where(
            {
                banana: 'a',
                tulip: {$ne: 'banana'},
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.banana\')) = \'a\' and JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.tulip\')) != \'banana\''
        )
        result = where(
            {
                $state: {$nin: ['new', 'old']},
                tulip: 'banana',
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE state not in (\'new\', \'old\') and JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.tulip\')) = \'banana\''
        )
    })
    it('should be able to make a not clause from a mongo query', async function () {
        let result = where(
            {
                banana: 'a',
                tulip: {$not: {$ne: 'banana'}},
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.banana\')) = \'a\' and not JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.tulip\')) != \'banana\''
        )
        result = where(
            {
                $state: {$nin: ['new', 'old']},
                tulip: 'banana',
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE state not in (\'new\', \'old\') and JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.tulip\')) = \'banana\''
        )
    })
    it('should handle subqueries', function () {
        let result = where(
            {
                banana: 'a',
                tulip: {
                    $joinsTo: {
                        table: 'something.something',
                        where: {
                            rainbow: 'colors',
                        },
                    },
                },
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.banana\')) = \'a\' and EXISTS (SELECT 1 FROM `something`.`something` WHERE JSON_UNQUOTE(JSON_EXTRACT(`something`.`something`.`data`, \'$.rainbow\')) = \'colors\' AND _id = JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.tulip\')))'
        )
    })
    it('should handle complex subs', function () {
        const query = {
            $and: [
                {
                    _deleted: null,
                    $or: [
                        {role: {$in: ['ceo']}},
                        {role: {$in: ['cfo', 'cto', 'cpo']}},
                        {
                            role: {
                                $in: ['dept_head', 'managing_director', 'snr_manager'],
                            },
                        },
                        {role: {$in: ['manager']}},
                        {role: {$in: ['team_lead']}},
                        {role: {$in: ['member', 'aux']}},
                    ],
                },
                {
                    $or: [
                        {_client: 0},
                        {_client: undefined},
                        {_client: null},
                        {_client: '572764'},
                    ],
                },
            ],
        }
        console.log(where(query))
    })
    it('should handle multiple elements in an or', function () {
        let result = where(
            {
                $or: [{a: 1}, {b: 1}],
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.a\')) = \'1\' or JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.b\')) = \'1\''
        )
    })
    it('should process this', function () {
        global.projectedColumns['database.table_task'] = {column: '_P_THIS'}
        const query = {
            $and: [
                {
                    $and: [
                        {
                            _deleted: null,
                            user: '59940924-a38e-4fae-9454-c1b9c8b7eb59',
                            $or: [{task: 'task2:hestia/tasks'}],
                        },
                        {
                            $or: [
                                {_client: 0},
                                {_client: undefined},
                                {_client: null},
                                {_client: '572764'},
                            ],
                        },
                    ],
                },
            ],
        }
        const result = where(query, 'data', 'database.table')
        console.log(result)
    })
    it('should handle elemMatch in an or', function () {
        let result = where(
            {
                _deleted: null,
                $or: [
                    {topics: {$elemMatch: {id: 1}}},
                    {topics: {$elemMatch: {id: 2}}},
                ],
            },
            'data',
            'test'
        )
        console.log(result)
    })
    it('should handle elemMatch', function () {
        let result = where(
            {
                banana: 'a',
                tulip: {
                    $elemMatch: {
                        test: 1,
                        id: 2,
                    },
                },
            },
            'data',
            'test'
        )
        expect(result).to.eq(
            'WHERE JSON_UNQUOTE(JSON_EXTRACT(`test`.`data`, \'$.banana\')) = \'a\' and JSON_CONTAINS(`test`.data, \'{"test":1,"id":2}\', \'$.tulip\') > 0'
        )
    })
})
