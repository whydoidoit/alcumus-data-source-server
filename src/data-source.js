const mysqlSource = require("./mysql-datasource")
const events = require("./events")

function getConfiguration(req) {
    const {params: {database, source = "none"}} = req
    let configuration = {database, source, functions: Object.assign({}, mysqlSource)}
    events.emit(`data-source.${database}.${source}`, configuration)
    return configuration.functions
}

async function getDataSource(req, res) {
    const {get} = getConfiguration(req)
    try {
        let results = await Promise.resolve(get(req, res))
        res.status(200).send(results)
    } catch (e) {
        console.error(e.stack)
        res.status(500).send(e.stack)
    }
}

async function mutateDataSource(req, res) {
    const {mutate} = getConfiguration(req)
    try {
        let results = await Promise.resolve(mutate(req, res))
        res.status(200).send(results)
    } catch (e) {
        console.error(e.stack)
        res.status(500).send(e.stack)
    }

}

async function updatesDataSource(req, res) {
    const {updates} = getConfiguration(req)
    try {
        updates(req, res)
    } catch (e) {
        console.error(e.stack)
        res.status(500).send(e.stack)
    }

}

async function mutateAll(req, res = null) {
    let groups = {}
    Object.entries(req.body).forEach(([key, value]) => {
        let parts = key.split(':')
        if (parts.length === 1) return
        let source = parts[1].split('/')
        if (source.length === 1) return
        let group = groups[source[0]] = groups[source[0]] || {}
        group[key] = value
    })
    let promises = []
    Object.entries(groups).forEach(([database, mutations]) => {
        let updatedReq = Object.assign({}, req, {params: {database}, body: mutations})
        const {mutate} = getConfiguration(updatedReq)
        promises.push(mutate(updatedReq, res))
    })
    await Promise.all(promises)
    res && res.status(200).send({ok: true})
}

module.exports = {
    initialize: function (app) {
        app.get('/data/:database/:source', getDataSource)
        app.post('/mutate/:database', mutateDataSource)
        app.get('/updates/:database', updatesDataSource)
        app.post('/mutate', mutateAll)
    },
    getDataSource,
    mutateDataSource,
    updatesDataSource,
    mutateAll
}

Object.defineProperty(module.exports, "events", {
    get() {
        return events
    }
})

require("./tables-datasource")
