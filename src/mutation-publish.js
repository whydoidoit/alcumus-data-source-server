const {publish} = require("./event-publish")
const events = require("./events")

events.on("mutation", function (database, mutation) {
    publish("mutate", mutation, database)
})

