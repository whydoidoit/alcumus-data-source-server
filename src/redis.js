const Redis = require("ioredis")
const genericPool = require('generic-pool')

let count = 1
function createClient(connection = process.env.REDIS) {
    return new Redis(connection)
}

let client = createClient()

const pool = []

const factory = {
    create() {
        return createClient()
    },
    destroy(client) {
        try {
            client.disconnect()
        } catch(e) {
            console.error(e)
        }
    }
}

const redisPool = genericPool.createPool(factory, {max: 200, min: 20, evictionRunIntervalMillis: 10000, numTestsPerEvictionRun: 10, idleTimeoutMillis: 20000})

async function get() {
    return await redisPool.acquire()
}

async function release(client) {
    try {
        return await redisPool.release( client )
    } catch(e) {
        console.trace("Error releasing", e)
    }
}

async function using(fn) {
    let redis = await get()
    try {
        return await Promise.resolve(fn(redis))
    } finally {
        await release(redis)
    }
}

const redis = createClient()
module.exports = {redis, createClient, get, release, using}
