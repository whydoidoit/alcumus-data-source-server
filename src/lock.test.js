process.env.DATABASE = "127.0.0.1"
process.env.DATABASE_PORT = "3307"
process.env.DATABASE_USERNAME = "root"
process.env.DATABASE_PASSWORD = ""
process.env.REDIS = "redis://127.0.0.1:6379"

const compose = require("docker-compose")
const {using} = require("./lock")
const {expect} = require("chai")
const path = require("path")

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout)
    })
}

describe("Lock tests", function () {
    before(async function () {
        try {
            // await compose.upAll({
            //     cwd: path.join(__dirname, ".."),
            //     config: path.join(__dirname, "..", "docker-compose.yml")
            // })
        } catch (e) {
            console.log(e.stack)
        }
    })
    after(async function () {
        // return compose.down({
        //     cwd: path.join(__dirname, ".."),
        //     config: path.join(__dirname, "..", "docker-compose.yml")
        // })
    })
    it("should be able to acquire a lock", async function() {
        let c = 1
        await using("banana", async function () {
            c = 2
        })
        expect(c).to.eq(2)
    })
    it("should not be able to acquire another lock until the first ends", async function () {
        let c = 0
        let d = 0
        async function test1() {
            return await using("bananas", async function() {
                d++
                await delay(600)
                expect(c).to.eq(0)
                c = 2
                return "1"
            })
        }

        async function test2() {
            await delay(400)
            return await using("bananas", async function() {
                d++
                expect(c).to.eq(2)
                return "2"
            })

        }
        await Promise.all([test1(), test2()])
        expect(d).to.eq(2)
    })
})
