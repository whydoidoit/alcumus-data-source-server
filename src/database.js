const msql = require('alcumus-mongo-sql')
const isString = require('lodash/isString')
const conditionBuilder = require('alcumus-mongo-sql/lib/condition-builder')
global.projectedColumns = {} // object tracking projected columns
const projectedColumnsPrefix = '__p_' //all projected columns will be prefixed with this value

msql.conditionalHelpers.add('$nin', {cascade: false}, function (column, set, values, collection, original) {
    if (Array.isArray(set) && set.length === 0) {
        return 'true'
    }
    return msql.conditionalHelpers.get('$in').fn(column, set, values, collection, original)
        .replace(new RegExp(' in', 'g'), ' not in')
        .replace(new RegExp(' is', 'g'), ' is not')
})

const mysql2 = require('mysql2')
let pool = mysql2.createPool({
    host: process.env.DATABASE,
    port: +process.env.DATABASE_PORT,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    connectionLimit: process.env.CONNECTION_LIMIT || 175,
}).promise()

async function refreshProjectedColumns() {
    const sql = `SELECT column_name as COLUMN_NAME, table_name as TABLE_NAME, table_schema as TABLE_SCHEMA FROM INFORMATION_SCHEMA.COLUMNS WHERE left(column_name,4) = '__p_'`
    const query = pool.query.bind(pool)
    const [cols] = await query(sql)
    try {
        cols.map((col) => {
            const columnPath = col.TABLE_NAME + '_' + col.COLUMN_NAME.replace(projectedColumnsPrefix, '')
            projectedColumns[columnPath] = {created: true, column: col.COLUMN_NAME}
        })
    } catch (e) {
        console.error('Error refreshing columns', cols)
    }
}

async function projectAndIndex(database, table, JSON_Extract_Definition, projectedColumnName, projectedColumnType = 'varchar(64)') {
    await require('./mysql-datasource').createTableIfNecessary(database, table)
    const columnPath = table + '_' + projectedColumnName
    if (!projectedColumns[columnPath]) {
        const query = pool.query.bind(pool)
        let sql = projectedColumnType !== 'boolean' ? `alter table ${database}.${table} add column ${projectedColumnsPrefix}${projectedColumnName} ${projectedColumnType} GENERATED ALWAYS AS (json_unquote( json_extract( data, '${JSON_Extract_Definition}' ) ))`
            : `alter table ${database}.${table} add column ${projectedColumnsPrefix}${projectedColumnName} ${projectedColumnType} GENERATED ALWAYS AS (if(json_unquote( json_extract( data, '${JSON_Extract_Definition}' ) )='true',1,0))`
        projectedColumns[columnPath] = {
            JSON_Extract_Definition,
            sql,
            idx: `CREATE INDEX idx_${projectedColumnsPrefix}${projectedColumnName} ON ${database}.${table} (${projectedColumnsPrefix}${projectedColumnName});`,
            created: false,
            column: `${projectedColumnsPrefix}${projectedColumnName}`
        }
        const [projectedColumnExists] = await query(`SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='${database}' AND TABLE_NAME='${table}' and column_name='${projectedColumnsPrefix}${projectedColumnName}'`)
        if (projectedColumnExists.length === 0) {
            //addColumnToDB
            await query(projectedColumns[columnPath].sql)
            //create index
            await query(projectedColumns[columnPath].idx)
        } else {
            //it already exists
        }
    }
    refreshProjectedColumns().catch(console.error)
}

async function whereAsync(definition, jsonColumn = 'data', table) {
    await refreshProjectedColumns()
    return where(definition, jsonColumn, table)
}

function clean(s) {
    return s.replace(/`/g, '').replace(/\+/g, '.')
}



function where(definition, jsonColumn = 'data', table) {
    definition = isString(definition) ? JSON.parse(definition) : definition
    mapName.jsonColumn = jsonColumn
    return `WHERE ${conditionBuilder(definition, table, [], mapName)}`

    function mapName(column, table) {
        const identifier = mysql2.escapeId(table) + '.' + mysql2.escapeId(jsonColumn)
        let extractPath = `${table}_${column}`
        if (projectedColumns[extractPath]) {
            return projectedColumns[extractPath].column
        }
        if (column.startsWith('$')) {
            return column.slice(1)
        }
        column = clean(column)
        if (column.startsWith('_ci_')) {
            return `CAST( JSON_UNQUOTE(JSON_EXTRACT(${identifier}, '$.${column.slice(4)}')) AS CHAR)`

        }

        return `JSON_UNQUOTE(JSON_EXTRACT(${identifier}, '$.${column}'))`
    }

}


function order(definition, jsonColumn = 'data') {
    return `ORDER BY ${definition.map(column => {
        let reverse = column.startsWith('-')
        if (reverse) column = column.slice(1)
        return (column.startsWith('$')
            ? column.slice(1)
            : `JSON_EXTRACT(${jsonColumn},'$.${column}')`)
            + (reverse ? ' DESC' : '')
    }).join(', ')}`
}

module.exports = {
    query: pool.query.bind(pool),
    escape: mysql2.escape,
    escapeId: mysql2.escapeId,
    format: mysql2.format,
    where,
    whereAsync,
    order,
    projectAndIndex
}



