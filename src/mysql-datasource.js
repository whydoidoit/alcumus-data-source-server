const {createClient} = require("./redis")
const {query, escapeId, escape, where} = require("./database")
const events = require("./events")
const {stringify, initialize} = require("alcumus-behaviours")
const {applyMutation} = require("alcumus-app-datasource-mutations")
const isObject = require("lodash/isObject")
require("alcumus-app-datasource-mutations/dist/stable-conflict-resolution")
const { subscribe } = require('alcumus-pub-sub')
const {retrieve} = require("./event-publish")
const {using} = require("./lock")
require("alcumus-mongo-sql")
const conditionBuilder = require("alcumus-mongo-sql/lib/condition-builder")

function typeFrom(key) {
    let type = key.split(':').pop().trim()
    return type.split('/').pop()
}

function databaseFrom(key, defaultValue) {
    let type = key.split(':').pop().trim()
    let parts = type.split('/')
    return parts.length > 1 ? parts : [defaultValue, parts[0]]
}

//Handle a creation event as if it were a new row, this
//won't stop other data mutations from being sent though
//TODO: Implement a method of hiding updates for a use
//      presumably by retrieving the item from the database
//      or a REDIS cache and checking it against security
events.on("shouldSend.mutate", async function (params) {
    let {message, req, context} = params
    if (message.type === 'create') {
        const [database, source] = databaseFrom(message._id)
        const eventParameters = {
            rows: [{data: message.state}],
                req,
            database,
            source,
            context
        }
        await events.emitAsync("will-return", eventParameters)
        if (eventParameters.rows.length === 0) {
            params.canProcess = false
        }
    }
})

let processing = {}

function createTableIfNecessary(database, source) {
    return query(`
      CREATE TABLE IF NOT EXISTS ${escapeId(database + "." + source)} (
            row_id INT NOT NULL AUTO_INCREMENT,
            _id VARCHAR(255) NOT NULL,
            data JSON,
            created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            state VARCHAR(255) GENERATED ALWAYS AS (
               json_unquote( json_extract( data, '$._behaviours.state' ) )
            ),
            deleted int GENERATED ALWAYS AS (
               json_unquote( json_extract( data, '$._deleted' ) )
            ),
            PRIMARY KEY (row_id),
            UNIQUE KEY (_id),
            KEY (state),
            KEY (deleted)
      )
    `)
}

async function get(req) {
    const {params: {database, source}} = req
    // language=MySQL
    await query(`
      CREATE DATABASE IF NOT EXISTS ${escapeId(database)}
    `)
    // language=MySQL
    await createTableIfNecessary(database, source)
    let {query: queryParameters = null} = req.query
    let whereClause = ""
    if (queryParameters) {
        whereClause = where(queryParameters, 'data', `${database}.${table}`)
    }
    // language=MySQL
    let [rows] = await query(`
        SELECT data FROM ${escapeId(database + "." + source)} ${whereClause}
        `)
    const eventParameters = {rows, req, database, source, context: {}}
    events.emit("will-return", eventParameters)
    rows = eventParameters.rows.map(row => row.data)
    let rowCount = rows.length
    let {skip, take} = req.query
    if (skip) rows = rows.slice(+skip, take ? +skip + +take : undefined)
    return {rows, rowCount}
}

async function getObject(key, context, database) {
    let source = typeFrom(key)
    ;[database] = databaseFrom(key, database)
    if (!context[key]) {
        //Ensure the source exists
        await query(`
          CREATE DATABASE IF NOT EXISTS ${escapeId(database)}
        `)
        await createTableIfNecessary(database, source)
        //Try to retrieve record
        let table = escapeId(database + "." + source)
        // language=MySQL
        let [rows] = await query(`
            SELECT row_id, data 
            FROM ${table} 
            WHERE _id = ${escape(key)}
        `)
        //Add a new row if necessary
        if (rows.length === 0) {
            let item = {_id: key}
            // language=MySQL
            let serialized = stringify(item)
            let [{insertId}] = await query(`
                INSERT INTO ${table}  
                    (_id, data) 
                    VALUES (${escape(key)}, ${escape(serialized)}) 
            `)
            rows.push({row_id: insertId, data: item})
        }
        let [row] = rows
        let {row_id} = row
        let object = initialize(row.data)

        context[key] = {
            object,
            row_id,
            save: async (processed) => {
                try {
                    await sendMessageAsync(object, "changed")
                    await query(`
                    UPDATE ${table} 
                       SET data = ${escape(stringify(object))} 
                    WHERE row_id = ${escape(row_id)}
                `)
                    await events.emitAsync(`committed.${object._id}`, processed, row_id)
                } catch (e) {
                    console.error(e)
                    throw e
                }
            }
        }
    }
    let {object} = context[key]
    return object
}

async function sendMessageAsync(object, message, ...params) {
    if (object.behaviours && object.behaviours.sendMessageAsync) await object.behaviours.sendMessageAsync(message, ...params)
}

async function process(req, key, mutation, context) {
    const {params: {database}} = req
    let object = await getObject(key, context, database)
    await sendMessageAsync(object, "willMutate", mutation)
    applyMutation(object, mutation, false)
    await sendMessageAsync(object, "hasMutated", mutation)
    events.emit("mutation", database, mutation)
}

async function mutate(req) {
    const {params: {database}, body: mutations} = req
    const context = {}
    // language=MySQL
    await query(`
      CREATE DATABASE IF NOT EXISTS ${escapeId(database)}
    `)

    //Try to find an existing item
    await Promise.all(Object.entries(mutations).map(async ([key, mutations]) => {
        //Make sure we can't process key updates while something else is
        await Promise.resolve(processing[key] || true)
        await using(`key.${key}`, async () => {
            let promise = processing[key] = new Promise(async function (resolve) {
                let processed = {}
                for (let mutation of mutations) {
                    await process(req, key, mutation, context)
                    let uq = Array.isArray(mutation.uq) ? mutation.uq : [mutation.uq]
                    uq.forEach(key => processed[key] = true)
                }
                await context[key].save(processed)
                resolve(true)
            })
            await promise
        })

    }))
    return {ok: true}
}


async function updates(req, res) {
    //In case the event handlers need to cache something - like already scanned items
    let context = {}
    req.setTimeout(0)

    async function write(message, {id, event, retry}) {
        let params = {canProcess: true, req, message, event, context}
        await events.emitAsync(`shouldSend.${event}`, params)
        if (!params.canProcess) return
        let toSend = JSON.stringify({event, message})
        let toWrite = []
        if (id) toWrite.push(`id: ${id}`)
        if (retry) toWrite.push(`retry: ${retry}`)
        if (message) toWrite.push(`data: ${toSend}`)
        res.write(toWrite.join('\n') + "\n\n")
    }

    res.set({
        "Content-Type": "text/event-stream",
        "Transfer-Encoding": "identity",
        "Cache-Control": "no-cache",
        "Connection": "keep-alive",
        "Access-Control-Allow-Origin": "*"
    })
    let first = true
    let lastEventId = req.headers["Last-Event-Id"] || req.query.lastEventId
    while (lastEventId) {
        try {
            let [{data, id, event}, next] = await retrieve(lastEventId)
            if (!first) await write(data, {id, event})
            first = false
            lastEventId = next
        } catch (e) {
            lastEventId = null
            await write("all", {event: "refresh-data"})
        }
    }


    let subscriber = createClient()
    const {params: {database}} = req
    await write(database, {event: "initialize", retry: 3000})
    subscriber.subscribe(`event.${database}`)
    subscriber.on("message", async function (channel, eventId) {
        // Message is the ID of a mutation
        try {
            let [{data, id, event}] = await retrieve(eventId)
            await write(data, {id, event})
        } catch (e) {
            //Presumably the mutation has expired
        }
    })
}

module.exports = {
    get,
    mutate,
    updates,
    createTableIfNecessary
}
